FROM php:7.2-cli

RUN apt-get update
RUN apt-get install -y zip zlib1g-dev libpq-dev mysql-client
RUN docker-php-ext-install zip mbstring pdo_mysql
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
RUN composer global require "laravel/installer"
RUN printf "\nPATH=\"$HOME/.composer/vendor/bin:\$PATH\"\n" | tee -a $HOME/.bashrc

RUN composer require "laravel-doctrine/orm:1.3.*"

RUN apt-get install -y locales
RUN echo "America/Sao_Paulo" > /etc/timezone && \
  dpkg-reconfigure -f noninteractive tzdata && \
  sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
  sed -i -e 's/# pt_BR.UTF-8 UTF-8/pt_BR.UTF-8 UTF-8/' /etc/locale.gen && \
  echo 'LANG="pt_BR.UTF-8"'>/etc/default/locale && \
  dpkg-reconfigure --frontend=noninteractive locales && \
  update-locale LANG=pt_BR.UTF-8

ENV LC_ALL=pt_BR.UTF-8
ENV LANG=pt_BR.UTF-8
ENV LANGUAGE=pt_BR.UTF-8

WORKDIR /var/www/html/
